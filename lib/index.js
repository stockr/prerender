var cluster = require('cluster')
  , fs = require('fs')
  , path = require('path')
  , http = require('http')
  , _ = require('lodash');

// Starts either a server or client depending on whether this is a master or
// worker cluster process
exports = module.exports = function(settings) {

  var server = require('./server');

  settings.isMaster = cluster.isMaster;
  settings.worker = cluster.worker;

  server.init(settings);

  if (cluster.isMaster) {
    console.log('Starting ' + settings.workers + ' workers listing on ' + settings.host + ':' + settings.port);
    for (var i = 0; i < settings.workers; i += 1) {
      // console.log('Starting worker thread #' + (i + 1));
      cluster.fork();
    }
    cluster.on('exit', function (worker) {
      console.log('Worker ' + worker.id + ' died.');
      // Spin up another to replace it
      console.log('Restarting worker thread...');
      cluster.fork();
    });
  } else {
    http
      .createServer(_.bind(server.onRequest, server))
      .listen(settings.port, settings.host, function () {
        //console.log('Worker running on ' + settings.host + ':' + settings.port);
      });
  }

  return server;
};

fs.readdirSync(__dirname + '/plugins').forEach(function(filename){
    if (!/\.js$/.test(filename)) {
      return;
    }
    var name = path.basename(filename, '.js');
    function load () {
      return require('./plugins/' + name);
    }
    Object.defineProperty(exports, name, {
      value: load
    });
});

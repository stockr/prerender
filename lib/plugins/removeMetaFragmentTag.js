var fragmentMatch = /\<meta\sname\=("|')?fragment("|')?\scontent\=("|')?!("|')?\>/i;

module.exports = {
  beforeSend: function (req, res, next) {
    var match
      , html;
    if (req.prerender.documentHTML) {
      html = req.prerender.documentHTML.toString();
      if (match = fragmentMatch.exec(html)) {
        req.prerender.documentHTML = html.replace(match[0], '');
      }
      next();
    } else {
      return next();
    }
  }
};
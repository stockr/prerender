#!/usr/bin/env node

var os = require('os')
  , _ = require('lodash')
  , prerender = require('./lib')
  , options = require('commander')
  , env = process.env
  , settings = {}
  , server;

options
  .option('-h, --host', 'specify the host [127.0.0.1]')
  .option('-p, --port', 'specify the port [3000]')
  .option('-w, --workers', 'number of phantom workers')
  .option('--phantomBasePort', 'phantom port')
  .option('--pageDoneCheckTimeout', '...')
  .option('--resourceDownloadTimeout', '...')
  .option('--waitAfterLastRequest', '...')
  .option('--jsTimeout', '...')
  .option('--jsCheckTimeout', '...')
  .option('--evaluateJavascriptCheckTimeout', '...')
  .option('--followRedirect', '...')
  .parse(process.argv);

options.host = options.host || env.HOST || '127.0.0.1';
options.port = options.port || env.PORT || 3000;
options.workers = options.workers || env.PHANTOM_CLUSTER_NUM_WORKERS || os.cpus().length;
options.phantomBasePort = options.phantomBasePort || env.PHANTOM_CLUSTER_BASE_PORT || 12300;
options.pageDoneCheckTimeout = options.pageDoneCheckTimeout || env.PAGE_DONE_CHECK_TIMEOUT || 100;
options.resourceDownloadTimeout = options.resourceDownloadTimeout || env.RESOURCE_DOWNLOAD_TIMEOUT || (10 * 2000);
options.waitAfterLastRequest = options.waitAfterLastRequest || env.WAIT_AFTER_LAST_REQUEST || 1000;
options.jsTimeout = options.jsTimeout || env.JS_TIMEOUT || (10 * 2000);
options.jsCheckTimeout = options.jsCheckTimeout || env.JS_CHECK_TIMEOUT || 100;
options.evaluateJavascriptCheckTimeout = options.evaluateJavascriptCheckTimeout || env.EVALUATE_JAVASCRIPT_CHECK_TIMEOUT || 100;
options.followRedirect = options.followRedirect || env.FOLLOW_REDIRECT || false;

server = prerender(options);

// Authentication (*is this necessary?)
// server.use(prerender.basicAuth());

// Enable/Restrict Domains
// server.use(prerender.whitelist());
// server.use(prerender.blacklist());

// Logging
// server.use(prerender.logger());

// HTML Modification
server.use(prerender.removeScriptTags());
server.use(prerender.removeMetaFragmentTag());

// Set Headers
server.use(prerender.httpHeaders());

// Caching Strategies
server.use(prerender.mongoDbCache());
// server.use(prerender.inMemoryHtmlCache());
// server.use(prerender.s3HtmlCache());

server.start();
